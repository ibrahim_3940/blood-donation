package ibrahim.muhammad.bloodbank.viewmodels


import ibrahim.muhammad.bloodbank.repository.DonateRepository
import androidx.lifecycle.ViewModel
import org.koin.core.KoinComponent
import org.koin.core.inject


class DonateViewModel : ViewModel(), KoinComponent {
    private val mRepository: DonateRepository by inject()

    fun applyNewDonor(donor: Map<String, Any>, userId: String) {
        mRepository.checkIfUserDonatedAndApplyIfNot(userId, donor)
    }

}
