package ibrahim.muhammad.bloodbank.viewmodels

import ibrahim.muhammad.bloodbank.models.Shoutout
import ibrahim.muhammad.bloodbank.repository.ShoutoutRepository
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import org.koin.core.KoinComponent
import org.koin.core.inject


class ShoutoutsViewModel : ViewModel(), KoinComponent {
    private val mRepository: ShoutoutRepository by inject()

    val shoutouts: LiveData<List<Shoutout>>?
        get() = mRepository.shoutouts

    fun createShoutout(shoutout: Map<String, Any>) {
        mRepository.createShoutout(shoutout)
    }

}
