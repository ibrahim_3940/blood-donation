package ibrahim.muhammad.bloodbank.viewmodels

import ibrahim.muhammad.bloodbank.models.Donor
import ibrahim.muhammad.bloodbank.repository.SearchRepository
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import org.koin.core.KoinComponent
import org.koin.core.inject


class SearchViewModel : ViewModel(), KoinComponent {
    private val mRepository: SearchRepository by inject()

    fun getSearchResult(city: String, bloodType: String): LiveData<List<Donor>> {
        return mRepository.searchForDonors(city, bloodType)
    }
}
