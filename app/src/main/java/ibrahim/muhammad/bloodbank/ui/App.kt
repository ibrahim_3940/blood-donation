package ibrahim.muhammad.bloodbank.ui


import ibrahim.muhammad.bloodbank.di.firebaseModule
import ibrahim.muhammad.bloodbank.di.preferenceModule
import ibrahim.muhammad.bloodbank.di.viewModelModule
import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            androidLogger(Level.DEBUG)
            modules(listOf(
                    firebaseModule,
                    viewModelModule,
                    preferenceModule))
        }
    }
}

