package ibrahim.muhammad.bloodbank.ui

import ibrahim.muhammad.bloodbank.R
import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat



class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.settings)
    }
}
