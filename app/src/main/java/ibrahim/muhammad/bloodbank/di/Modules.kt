package ibrahim.muhammad.bloodbank.di

import ibrahim.muhammad.bloodbank.repository.AuthenticationRepository
import ibrahim.muhammad.bloodbank.repository.DonateRepository
import ibrahim.muhammad.bloodbank.repository.SearchRepository
import ibrahim.muhammad.bloodbank.repository.ShoutoutRepository
import ibrahim.muhammad.bloodbank.utils.PreferenceUtils
import ibrahim.muhammad.bloodbank.viewmodels.AuthenticationViewModel
import ibrahim.muhammad.bloodbank.viewmodels.DonateViewModel
import ibrahim.muhammad.bloodbank.viewmodels.SearchViewModel
import ibrahim.muhammad.bloodbank.viewmodels.ShoutoutsViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val firebaseModule = module {

    factory { FirebaseAuth.getInstance().currentUser }
    single { FirebaseMessaging.getInstance() }
    single { FirebaseAuth.getInstance() }
    single { FirebaseFirestore.getInstance() }
}

val viewModelModule = module {

    factory { AuthenticationRepository() }
    viewModel { AuthenticationViewModel() }

    factory { DonateRepository() }
    viewModel { DonateViewModel() }

    factory { SearchRepository() }
    viewModel { SearchViewModel() }

    factory { ShoutoutRepository() }
    viewModel { ShoutoutsViewModel() }
}

val preferenceModule = module {
    single { PreferenceUtils() }
}

